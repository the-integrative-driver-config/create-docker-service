# Create docker service

I use this project as an template in combination with this project to [backup docker](https://gitlab.com/the-integrative-driver-config/backup-docker) volumes.

## Build recipes
``` bash
Available recipes:
    default  # Default chooser
    docs     # Copy recipes to docs
    down     # stop and remove docker compose
    include  # Include docker files to test
    new name # Create new compose project
    up       # Run docker compose
```
