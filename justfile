# Default chooser
default:
  @just --choose

# Run docker compose
up:
  docker compose --file .docker-compose.yml up -d

# stop and remove docker compose
down:
  docker compose --file .docker-compose.yml down --remove-orphans

# Create new compose project
new name:
  mkdir {{name}}
  touch {{name}}/docker-compose.yml
  nvim {{name}}/docker-compose.yml

# Include docker files to test
include:
  #!/usr/bin/python3
  import yaml
  from pathlib import Path

  path_config = ".docker-compose.yml"

  with open(path_config, "r") as config:
      content = yaml.safe_load(config)
  current_dir = Path.cwd()
  docker_files = []
  for path in current_dir.rglob('docker-compose.yml'):
      if path.is_file() and path.parent != current_dir:
          docker_files.append(str(path.relative_to(current_dir)))
  content["include"] = docker_files
  with open(path_config, "w") as config_new:
      yaml.dump(content, config_new)

# Copy recipes to docs
docs:
  #!/bin/bash
  just -l | xclip -sel clip
  nvim README.md +/Available recipes:
